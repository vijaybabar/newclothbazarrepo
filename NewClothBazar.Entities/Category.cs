﻿using System.Collections.Generic;

namespace NewClothBazar.Entities
{
    public class Category : BaseEntity
    {
        public List<Product> Products { get; set; } 
    }
}
