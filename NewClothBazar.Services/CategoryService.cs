﻿using NewClothBazar.Database;
using NewClothBazar.Entities;
using System.Collections.Generic;
using System.Linq;

namespace NewClothBazar.Services
{
    public class CategoryService
    {
        private readonly NewClothBazarContext db;

        public CategoryService()
        {
            this.db = new NewClothBazarContext();
        }

        public List<Category> GetCategories()
        {
            return this.db.Categories.ToList();
        }

        public Category GetCategoryById(int id)
        {
            return this.db.Categories.FirstOrDefault(x => x.Id == id);
        }

        public void SaveCategory(Category category)
        {
            this.db.Categories.Add(category);
            this.db.SaveChanges();
        }

        public void UpdateCategory(Category category)
        {
            this.db.Entry(category).State = System.Data.Entity.EntityState.Modified;
            this.db.SaveChanges();
        }

        public void DeleteCategory(Category category)
        {
            this.db.Categories.Remove(GetCategoryById(category.Id));

            this.db.SaveChanges();
        }
        
    }
}
