﻿
using NewClothBazar.Entities;
using System.Data.Entity;

namespace NewClothBazar.Database
{
    public class NewClothBazarContext : DbContext
    {
        public NewClothBazarContext() : base("NewClothBazarCS")
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }    
    }
}
