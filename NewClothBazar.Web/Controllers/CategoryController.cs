﻿using NewClothBazar.Entities;
using NewClothBazar.Services;
using System.Linq;
using System.Web.Mvc;

namespace NewClothBazar.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly CategoryService categoryService;

        public CategoryController()
        {
            this.categoryService = new CategoryService();
        }

        [HttpGet]
        public ActionResult Index()
        {
            var category = this.categoryService.GetCategories();

            return View(category);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Category category)
        {
            this.categoryService.SaveCategory(category);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var category = this.categoryService.GetCategoryById(id);

            return View(category);
        }

        [HttpPost]
        public ActionResult Edit(Category category)
        {
            this.categoryService.UpdateCategory(category);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var category = this.categoryService.GetCategoryById(id);

            return View(category);
        }

        [HttpPost]
        public ActionResult Delete(Category category)
        {
            this.categoryService.DeleteCategory(category);

            return RedirectToAction("Index");
        }
    }
}